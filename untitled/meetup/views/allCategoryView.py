from rest_framework import viewsets
from meetup.models.Category import Category
from meetup.serializers import CategorySerializer
from rest_framework.response import Response

class AllCategorySet(viewsets.ViewSet):
    def list(self, request):
        queryset = Category.objects.order_by('-content')
        serializer_class = CategorySerializer(queryset, many=True)
        return Response(serializer_class.data)


