from meetup.models.EventLike import EventLike, Event
from meetup.models.EventVisitor import EventVisitor
from meetup.models.CategoryEvent import CategoryEvent, Category
from meetup.models.Comment import Comment
from meetup.models.EventImage import EventImage
from meetup.serializers import EventSerializer, EventDetailSerializer, CommentSerializer, UserSerializer, EventImageSerializer
from rest_framework import viewsets
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import filters

from django.core import serializers

from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory


import datetime

class CustomPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'

class AllEventSet(viewsets.ReadOnlyModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title',)


    @action(methods=['post'], detail=False)
    def search_Event_Date_Range(self, request):
        queryset = Event.objects.filter(timeEvent__range=(request.POST.get('start'), request.POST.get('end')))
        serializer_class = EventSerializer(queryset, many=True)
        return Response(serializer_class.data)

    @action(methods=['get'], detail=True, pagination_class=CustomPagination)
    def search_category(self, request, pk=None):
        if not pk.isdigit():
            return Response('Do not have category')
        category = Category.objects.filter(id=pk)
        if category.exists():
            category_events = CategoryEvent.objects.filter(idCategory__id=pk)
            allEvent = []
            for event in category_events:
                allEvent.append(event.idEvent)
            page = self.paginate_queryset(allEvent)
            if page is not None:
                serializer = EventSerializer(page, many=True)
                return self.get_paginated_response(serializer.data)
            serializer = EventSerializer(allEvent, many=True)
            return Response(serializer.data)
        else:
            return Response('Do not have category')

    def retrieve(self, request, pk=None):
        if not pk:
            return Response('Do not have event')
        if not pk.isdigit():
            return Response('Do not have event')
        queryset = Event.objects.filter(id=pk)
        if queryset.exists():
            serializer_class = EventDetailSerializer(queryset.first(), many=False)
            return Response(serializer_class.data)
        else:
            return Response("Do not have event")

    @action(methods=['get'], detail=True, pagination_class=CustomPagination)
    def comment(self, request, pk=None):
        queryset = Comment.objects.filter(event__id=pk)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = CommentSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = CommentSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def number_of_like(self, request, pk=None):
        if not pk.isdigit():
            return Response('Do not have event')
        event = Event.objects.filter(id=pk)
        if event.exists():
            number_of_like = EventLike.objects.filter(idEvent=pk).count()
            return Response(number_of_like)
        else:
            return Response('Do not have this event')

    @action(methods=['get'], detail=True)
    def list_visitor(self, request, pk=None):
        if not pk.isdigit():
            return Response('Do not have event')
        event = Event.objects.filter(id=pk)
        if event.exists():
            queryset = EventVisitor.objects.filter( idEvent__id=pk)
            allVisitor = []
            for eventVisitor in queryset:
                allVisitor.append(eventVisitor.idVisitor)
            # serializer_class = UserSerializer(allVisitor, many=True)
            # return Response(serializer_class.data)
            page = self.paginate_queryset(allVisitor)
            if page is not None:
                serializer = UserSerializer(page, many=True)
                return self.get_paginated_response(serializer.data)
            serializer = EventSerializer(allVisitor, many=True)
            return Response(serializer.data)
        else:
            return Response('Do not have this event')

