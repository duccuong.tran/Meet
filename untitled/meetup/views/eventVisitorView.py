from meetup.models.EventVisitor import EventVisitor
from meetup.models.EventLike import EventLike, Event
from meetup.models.Comment import Comment
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.auth.models import Permission, User
from django.db import transaction
import datetime

class EventVisitorSet(viewsets.ViewSet):
    @action(methods=['post'], detail=False)
    def join(self, request):
        if request.user.is_authenticated:
            user_id = request.session['user_id']
            event_visit = EventVisitor.objects.filter(idVisitor=user_id, idEvent=request.POST['idEvent'])
            if event_visit.exists():
                event_visit.delete()
                return Response('You leave event')
            else:
                event = Event.objects.filter(id=request.POST.get('idEvent')).first()
                visitor = User.objects.filter(id=user_id).first()
                event_visit = EventVisitor(idEvent=event, idVisitor=visitor)
                event_visit.save()
                return Response('You join event')
        else:
            return Response('You not logged in')

    @action(methods=['post'], detail=False)
    def like(self, request):
        if request.user.is_authenticated:
            user_id = int(request.session['user_id'])
            idEvent = request.POST.get('idEvent')
            if not idEvent:
                return Response('idEvent can not null')
            if not idEvent.isdigit():
                return Response('Do not have Event')
            event_visit = EventLike.objects.filter( idVisitor__id=user_id, idEvent__id=request.POST['idEvent'])
            if event_visit.exists():
                event_visit.delete()
                return Response('You liked event')
            else:
                event = Event.objects.filter(id=request.POST.get('idEvent')).first()
                visitor = User.objects.filter(id=user_id).first()
                event_visit = EventLike(idVisitor=visitor, idEvent=event)
                event_visit.save()
                return Response('You cancel like event')
        else:
            return Response('You not logged in')

    @action(methods=['post'], detail=False)
    def post_comment(self, request):
        if request.user.is_authenticated:
            user_id = request.session['user_id']
            visitor = User.objects.filter(id=user_id).first()
            idEvent = request.POST.get('idEvent')
            content = request.POST.get('content')
            if not idEvent:
                return Response('idEvent can not null')
            if not idEvent.isdigit():
                return Response('Do not have Event')
            if not content:
                return Response('Comment can not null')
            event = Event.objects.filter(id=idEvent).first()
            comment = Comment(visitor=visitor, content=content, event=event)
            comment.save()
            return Response('You recent comment ' + request.POST.get('content'))
        else:
            return Response('You not logged in')

    @action(methods=['post'], detail=False)
    def edit_comment(self, request):
        if request.user.is_authenticated:
                idComment = request.POST.get('idComment')
                content = request.POST['content']
                if not idComment:
                    return Response('idComment can not null')
                if not idComment.isdigit():
                    return Response('Do not have Comment')
                if not content:
                    return Response('Comment can not null')
                comment = Comment.objects.filter(id=idComment)
                if comment.exists():
                    comment = comment.first()
                    if comment.visitor.id == request.session['user_id']:
                        comment.content = content
                        comment.updated_at = datetime.datetime.now()
                        comment.save()
                        return Response('You edited comment to' + content)
                    else:
                        return Response('You can not edit this comment')
                else:
                    return Response('Do not have this comment')
