from django.db import models
from django.contrib.auth.models import User
from meetup.models.Event import Event
import datetime
class Comment(models.Model):
    id = models.AutoField(primary_key=True, db_index=True)
    content = models.CharField(max_length=1000)
    visitor = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    created_at =  models.DateTimeField(default=datetime.datetime.now(), null=False)
    updated_at =  models.DateTimeField(default=datetime.datetime.now(), null=False)

    class Meta:
        ordering = ['-created_at']
        indexes = [
            models.Index(fields=['id']),
            models.Index(fields=['event'])
        ]


