from django.db import models
from meetup.models.Event import Event
import datetime
class EventImage(models.Model):
    id = models.AutoField(primary_key=True, db_index=True)
    url = models.CharField(max_length=1000)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='images')
    created_at =  models.DateTimeField(default=datetime.datetime.now(), null=False)

    class Meta:
        ordering = ['-created_at']
        indexes = [
            models.Index(fields=['id']),
            models.Index(fields=['event'])
        ]
