from django.db import models

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.CharField(null=False, max_length=255)
    image = models.CharField(max_length=1000)

