from django.conf.urls import url, include
from rest_framework import routers
from meetup.views import allEventView

# app_name = 'meetup'


router = routers.DefaultRouter()
router.register(r'allEvent', allEventView.AllEventSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]